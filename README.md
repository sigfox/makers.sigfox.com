#makers.sigfox.com

##Dependencies

###Ruby

[RVM](https://rvm.io/rvm/install) is recommended as you won't have to deal with privileges issues & wil be able to manage the version used

####Install rvm

```
$ \curl -sSL https://get.rvm.io | bash -s stable
$ source ~/.rvm/scripts/rvm
$ rvm install 2.1
$ rvm use 2.1
```

###Jekyll

Follow the [Quick start](http://jekyllrb.com/docs/quickstart/) guide from jekyll.com

```
$ gem install jekyll
```

##Build & Run Locally

```
$ jekyll serve
```
